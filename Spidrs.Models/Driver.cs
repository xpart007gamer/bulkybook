﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Spidrs.Models
{
    [Table("Drivers")]
    public class Driver
    {
        [Key]
        public int DriverId { get; set; }
        public string BackOfficeCustomerID { get; set; }
        public string EmailId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TempPassword { get; set; }
        public string FattMerchantCustomerID { get; set; }
        public bool CheckTerms { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public bool AllowTextMessage { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        
        public bool AllowCreditCard { get; set; }
        public bool? OnlineEnabled { get; set; }
        public bool? InvtSentMail { get; set; }
        public DateTime InvtMailSentDate { get; set; }
        public bool IsParent { get; set; }
        public bool IsChild { get; set; }
        public string AspNetUsersId { get; set; }
        public string Phone1Type { get; set; }
        public bool? Phone1Text { get; set; }
        public string Phone2Type { get; set; }
        public bool? Phone2Text { get; set; }
        public bool invoice { get; set; }
        public int billingstatus { get; set; }
        public int? Ended { get; set; }

        public int inactive { get; set; }

        public int collections { get; set; }
    }
}
