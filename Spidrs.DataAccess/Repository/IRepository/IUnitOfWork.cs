﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.DataAccess.Repository.IRepository
{
    public interface IUnitOfWork
    {
     
      
     
        IApplicationRepository ApplicationUser { get; }

        void Save();
    }
}
