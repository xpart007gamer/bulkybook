﻿using Spidrs.DataAccess.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _db;

        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
         
          
      
            ApplicationUser = new ApplicationRepository(_db);
        }
     
       
   
        public IApplicationRepository ApplicationUser { get; private set; }

        public void Save()
        {
            _db.SaveChanges();
        }

    }
}
