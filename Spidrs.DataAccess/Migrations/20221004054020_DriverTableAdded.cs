﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Spidrs.DataAccess.Migrations
{
    public partial class DriverTableAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Drivers",
                columns: table => new
                {
                    DriverId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BackOfficeCustomerID = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EmailId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TempPassword = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FattMerchantCustomerID = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CheckTerms = table.Column<bool>(type: "bit", nullable: false),
                    Phone1 = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Phone2 = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AllowTextMessage = table.Column<bool>(type: "bit", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AllowCreditCard = table.Column<bool>(type: "bit", nullable: false),
                    OnlineEnabled = table.Column<bool>(type: "bit", nullable: true),
                    InvtSentMail = table.Column<bool>(type: "bit", nullable: true),
                    InvtMailSentDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsParent = table.Column<bool>(type: "bit", nullable: false),
                    IsChild = table.Column<bool>(type: "bit", nullable: false),
                    AspNetUsersId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Phone1Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Phone1Text = table.Column<bool>(type: "bit", nullable: true),
                    Phone2Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Phone2Text = table.Column<bool>(type: "bit", nullable: true),
                    invoice = table.Column<bool>(type: "bit", nullable: false),
                    billingstatus = table.Column<int>(type: "int", nullable: false),
                    Ended = table.Column<int>(type: "int", nullable: true),
                    inactive = table.Column<int>(type: "int", nullable: false),
                    collections = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drivers", x => x.DriverId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Drivers");
        }
    }
}
