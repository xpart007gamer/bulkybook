﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Spidrs.Web.Areas.Admin.Controllers.ApiControllers
{
    [Area("Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class TestApi : ControllerBase
    {
        // GET: api/<TestApi>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<TestApi>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<TestApi>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<TestApi>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<TestApi>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
