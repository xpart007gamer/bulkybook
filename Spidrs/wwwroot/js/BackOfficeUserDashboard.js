﻿$(document).ready(function () {
    $("#ddlskip").hide();
   
    
    
});

//Advance Search
function AdvanceSearchPOP() {

    $("#MyModalAdvanceSearch").show();
}
function radioName(Name) {
    $("#lblAdvanceSearch").text(Name);
    var radioValue = $("input[name='radioName']:checked").val();
    if (Name == 'Skip') {
        $("#txtAdvanceSearch").hide();
        $("#txtAdvanceSearch").val('');
        $("#ddlskip").show();
    }
    else {
        $("#ddlskip").hide();
        $("#txtAdvanceSearch").show();
    }

}
function AdvanceSearchPOPClose() {
    $("#MyModalAdvanceSearch").hide();
}
function CustomerHistoryPOPClose() {
    $("#MyModalCustomerHistory").hide();
}
function AdvanceSearchValue(){
    var Value = "";


    var ColumnName = $("input[name='radioName']:checked").val();
    if (ColumnName == undefined) {
        $('#txtAdvanceSearch').css('border-color', 'red');
        $('#txtAdvanceSearch').focus();
        return false;
    }
    if (ColumnName == 'SkipService') {
        Value = $('#ddlskip').val();
    }
    else {
        Value = $('#txtAdvanceSearch').val();
    }

    $('#txtAdvanceSearch').css('border-color', 'none');


    var hdAdvanceSearch = $('#hdAdvanceSearch').val();
    $.ajax({
        type: "Post",
        url: hdAdvanceSearch,
        data: { ColumnName: ColumnName, Value: Value },
        dataType: "json",
        success: function (response) {

            $('#tblBackOfficeUserDashboard').dataTable({
                data: response,
                "columns": [



                    { "data": "backOfficeCustomerID" },
                    { "data": "name" },
                    { "data": "emailId" },
                    { "data": "serviceAccountNumber" },
                    { "data": "address" },
                    {
                        "render": function (data, type, full, meta) {
                            return ' <a onclick="CustomerHistory(' + full.backOfficeCustomerID + ');"><i class="fa fa-eye" style="color:#337ab7"></i></a>';
                           
                        }
                    }


                ],

                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 10,
                "bDestroy": true
            });
            
            setTimeout(function () {

            }, 1000);
        },
        error: function (response) {

        }
    });
}
function CustomerHistory(BackOfficeCustomerID) {
   
    $('#MyModalCustomerHistory').show();
  
    var hdCustomerinfo = $('#hdCustomerinfo').val();
    $.ajax({
        type: "Post",
        url: hdCustomerinfo,
        data: { BackOfficeCustomerID: BackOfficeCustomerID},
        dataType: "json",
        success: function (response) {
            $('#lblFirstName').text(response.customerStatementHistoryModel.firstName);
            $('#lblLastName').text(response.customerStatementHistoryModel.lastName);
            $('#lblBackOfficeID').text(response.customerStatementHistoryModel.backOfficeCustomerID);
            $('#lblEmailId').text(response.customerStatementHistoryModel.emailId);
            $('#lblAddress1').text(response.customerStatementHistoryModel.address1);
            $('#lblAddress2').text(response.customerStatementHistoryModel.address2);
            $('#lblState').text(response.customerStatementHistoryModel.state);
            $('#lblCity').text(response.customerStatementHistoryModel.city);
            $('#lblPhone1').text(response.customerStatementHistoryModel.phone1);
            $('#lblPhone2').text(response.customerStatementHistoryModel.phone2);
           
            $('#example').dataTable({
                data: response.recentStatement,
                "columns": [

                  
                    {
                        "data": "statementDate",
                        "render": function (data) {
                            var date = new Date(data);
                            var month = date.getMonth() + 1;
                            return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                        }
                    },
                    
                    { "data": "statementID" },
                    { "data": "serviceTypeFullName" },
                    { "data": "serviceAccountNumber" },
                    
                    {
                        "data": "dueDate",
                        "render": function (data) {
                            var date = new Date(data);
                            var month = date.getMonth() + 1;
                            return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                        }
                    },
                    { "data": "amountBilled" },
                    

                ],

                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 4,
                "bDestroy": true
            });
            $('#example1').dataTable({
                data: response.paymentModelList,
                "columns": [


                    {
                        "data": "paymentDate",
                        "render": function (data) {
                            var date = new Date(data);
                            var month = date.getMonth() + 1;
                            return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                        }
                    },

                    { "data": "serviceAccountNumber" },
                    { "data": "serviceAccountName" },
                    { "data": "amount" },
                    //{ "data": "bankName" },


                ],

                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 4,
                "bDestroy": true
            });
           
            setTimeout(function () {

            }, 1000);
        },
        error: function (response) {

        }
    });
   
}

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
