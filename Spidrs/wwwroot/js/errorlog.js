﻿$(document).ready(function () {
    if ($.fn.DataTable.isDataTable('#tblErrorLog')) {
        $('#tblErrorLog').DataTable().destroy();
    };
    $("#tblErrorLog").DataTable({
        order: [[0, 'desc']],
        language: {
            "search": '<i class="fa fa-search"></i>',
            searchPlaceholder: "Search records",
            paginate: {
                next: 'Next &raquo;',
                previous: '&laquo; Previous'
            },
            "processing": '<div class="loader-wrapper show1"><div class= "loader" ></div></div>'
        },
        "pagingType": "input",
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "destroy": true,
        "lengthChange": false,
        "ordering": true,
        "ajax": {
            "url": $('#hdErrorLogReport').val(),
            "type": "POST",
            "datatype": "json"
        },
        columns: [
            { "data": "createdOnDateTimeString", width: 200 },
            { "data": "message" },
            {
                "data": "stackTrace", sortable: false, render: function (stackTrace) {
                    return "<span title='" + stackTrace + "' >" + (stackTrace || '').substring(0,30) + "</span>";
                }
            },
            { "data": "rawDetail", sortable: false },
            {
                render: function () {
                    return "";
                },
                sortable: false,
                width: 100
            }
        ],
        iDisplayLength: 25
    });
});