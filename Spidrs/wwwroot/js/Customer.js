﻿$(document).ready(function () {
   
    if ($.fn.DataTable.isDataTable('#tblcustomer')) {
        $('#tblcustomer').DataTable().destroy();
    }  
  

   

    
    
    $("#tblcustomer").DataTable({

        language: {
            "search": '<i class="fa fa-search"></i>',
            searchPlaceholder: "Search records",
            paginate: {
                next: 'Next &raquo;',
                previous: '&laquo; Previous'
            },
            "processing": '<div class="loader-wrapper show1"><div class= "loader" ></div></div>'
        },
        "pagingType": "input",
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "destroy": true,
        "lengthChange": false,
        "ordering": true,

        "ajax": {
            "url": '/Customers/GetCustomerDetails',
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            {
                "data": "backOfficeCustomerID", "name": "backOfficeCustomerID", "autoWidth": true,
                mRender: function (data, type, full) {
                    if (full.isParent == true) {
                        return " " + data + " <img class='joinIcon' src='/img_new/parents.png' />";
                    }
                    if (full.isChild == true) {
                        return " " + data + " <img class='joinIcon' src='/img_new/childs.png' />";
                    }
                    if (full.isParent == false && full.isChild == false) {
                        return " " + data + "";
                    }
                }
            }
            , { "data": "firstName", "name": "firstName", "autoWidth": true }
            , { "data": "serviceAddress", "name": "serviceAddress", "autoWidth": true }
            , { "data": "billingCity", "name": "billingCity", "autoWidth": true }
            , { "data": "emailId", "name": "emailId", "autoWidth": true }
            , { "data": "phone", "name": "phone", "autoWidth": true }
            , { "data": "phone2", "name": "phone2", "autoWidth": true }
            , {
                "render": function (data, type, full, meta) {
                    if ((full.onlineEnabled == null || full.onlineEnabled == false) && (full.invtSentMail == null || full.invtSentMail == false)) {
                        if (full.aspNetUsersId == null || full.aspNetUsersId == "") {
                            return '<a onclick="SendCustomerMail(\'' + full.emailId + '\',\'' + full.backOfficeCustomerID + '\');return false;"><img style="height:25px; width:20px; cursor:pointer;" src="/img_new/messageicon.png" /></a>';
                        }
                        else {
                           // return '<span class="fa fa-envelope envelope-color" style="height:25px; width:20px;" disabled></span><span><a href="~/Customers/RedirectOnLogin/?Email=\'' + full.emailId + '\'><i class="fa fa-sign-in" style="color:#337ab7"></i></a></span>';
                            return ' <span class="fa fa-envelope envelope-color" style="height:25px; width:20px;" disabled></span> <span><a href="/Customers/RedirectOnLogin/?Email=' + full.emailId + '"><i class="fa fa-sign-in" style="color:#337ab7"></i></a></span>';
                        }

                    }
                    else {

                        if (full.aspNetUsersId == null || full.aspNetUsersId == "") {
                            if (full.invtMailSentMin > 30) {
                                return '<a onclick="SendCustomerMail(\'' + full.emailId + '\', \'' + full.backOfficeCustomerID + '\');return false;"><img style="height:25px; width:20px; cursor:pointer;" src="/img_new/messageicon.png" /></a>';
                            }
                            else {
                                return ' <a onclick="SendCustomerMail(\'' + full.emailId + '\',\'' + full.backOfficeCustomerID + '\');return false;"> <i class="fa fa-envelope envelope-color" style="color:#337ab7;cursor:pointer;"></i></a>';
                            }
                        }
                        else {
                            return ' <span class="fa fa-envelope envelope-color" style="height:25px; width:20px;" disabled></span> <span><a href="/Customers/RedirectOnLogin/?Email=' + full.emailId + '"><i class="fa fa-sign-in" style="color:#337ab7"></i></a></span>';
                            

                        }

                    }
                }
            }


        ]

         
    });
    // $(".loader-wrapper").removeClass("show1");
    
    $.ajax({
        url: '/Customers/GetCustomerCountforLogin',
        method: "GET",
        dataType: "json",
        success: function (data) {
            $('#lbltotalactive').text(data.loginActive);
            $('#lbltotalpaperless').text(data.paperless);
             }
    });

    SearchText();
   
});

function SearchText() {



    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary

    ;
    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

}
function SendCustomerMail(customeremail, BackOfficeCustomerID) {

    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(customeremail)) {
        swal({

            text: "Invalid email id please check the email format.",
            icon: "error",

        })
            .then((ok) => {
                if (ok) {


                }
            });
        return false;
    }

   
    var url = $('#hdGetCustomerName').val();
    ;
    $.ajax({
        type: "Post",
        url: url,
        data: { BackOfficeCustomerID: BackOfficeCustomerID},
        success: function (result) {
            ;
            $('#hfcustomeremail').val(customeremail);
            $('#hfBackOfficeCustomerID').val(BackOfficeCustomerID);
            $('#lblcustomername').text(result);
            $('#MyModalMail').modal('show');
        },
        error: function (response) {
        }
    });

  
}
function SendMailCustomer() {
   
    var customeremail = $('#hfcustomeremail').val();

    var BackOfficeCustomerID = $('#hfBackOfficeCustomerID').val();
    var url = $('#hdsnSendMailCustomers').val();
    $.ajax({
        type: "Post",
        url: url,
        data: { customeremail: customeremail, BackOfficeCustomerID: BackOfficeCustomerID },
        success: function (result) {
            if (result == true) {
                $("#MyModalMail").modal("hide")
                $("div.success").show();
                $("div.success").fadeIn(300).delay(1500).fadeOut(4000);
                setTimeout(function () {
                    var url = $("#hdcustomer").val();
                    window.location.href = url;
                }, 3000);
            }
            else {
                $("#MyModalMail").modal("hide")
                $("div.failure").show();
                $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                setTimeout(function () {
                    var url = $("#hdcustomer").val();
                    window.location.href = url;
                }, 3000);
            }
        },
        error: function (response) {
        }
    });
}



