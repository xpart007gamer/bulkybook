﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Spiders.Api.Model;
using Spidrs.DataAccess;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Spiders.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        public IConfiguration _configuration;
        private readonly ILogger<LoginController> _logger;
        public readonly ApplicationDbContext _context;

        public LoginController(ILogger<LoginController> logger, IConfiguration configuration, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
            _configuration = configuration;
        }




        [HttpPost(Name = "GetJwt")]
        public User Post(string UserName,string Password)
        {
            var jwt =  GenerateJwtToken(UserName,Password);
          
                var userData = GetUser(UserName, Password);
                var userInfo = new User()
                {
                    UserName = userData.UserName,
                    Email = userData.Email,
                    PhoneNumber = userData.PhoneNumber,
                    JwtToken = jwt,
                };


                return userInfo;
          
        }

        
        internal  string GenerateJwtToken(string UserName, string Password)
        {
            if (UserName != null && Password != null)
            {
                var userData = GetUser(UserName, Password);
                var jwt = _configuration.GetSection("Jwt").Get<Jwt>();
                if (userData != null)
                {
                    var claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, jwt.Subject),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                  
                        new Claim("UserName", UserName),
                        new Claim("Password", Password)

                    };
                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwt.key));
                    var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    var token = new JwtSecurityToken(
                       jwt.Issuer,
                       jwt.Audience,
                        claims,
                        expires: DateTime.Now.AddMinutes(20),
                        signingCredentials: signIn
                    );
                    return new JwtSecurityTokenHandler().WriteToken(token).ToString();
                }
                else
                {
                    return "";
                }


            }
            else
            {
                return "";
            }
        }

        internal User GetUser(string username, string password)
        {
             var user = _context.Users.FirstOrDefault(u => u.UserName == username);
          
                var userInfo = new User()
                {
                    UserName = user.UserName,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                    
                    
                };
                return userInfo;
           
           
                 
        }
    }
}
